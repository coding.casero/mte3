/* 
    Product Inventory Management

    Given an array of product objects, where each object contains the following properties: name, category, price, and quantity.

 Write a function called getTotalInventoryValue that takes the array of product objects as input and returns the total value of the entire inventory.
*/

const products = [
    { name: 'Laptop', category: 'Electronics', price: 1000, quantity: 5 },
    { name: 'Shirt', category: 'Clothing', price: 20, quantity: 10 },
    { name: 'Book', category: 'Books', price: 15, quantity: 25 },
    { name: 'Headphones', category: 'Electronics', price: 50, quantity: 8 }
  ];

function getTotalInventoryValue(products) {  
    //Declarative approach: return products.map((x) => x.price *= x.quantity).reduce((a,b) => a + b)
    let result = 0

    for(let x of products) {
        result += (x.price * x.quantity)
    }
    return result
}


/*  
    User Validation

    Given an array of user objects, where each object contains the following properties: username and password. 
    Write a function called isUserValid that takes the array of user objects and a username/password combination as input and returns true if the combination is valid, and false otherwise. 
  
  */

    const users = [
        { username: 'alice', password: 'pass123' },
        { username: 'bob', password: 'pass456' },
        { username: 'charlie', password: 'pass789' }
      ];

function isUserValid(users, username, password) {
            
    for (let people of users) {
        if (people.username == username && people.password == password) {
            return true
            }
        }
        return false
    }
  
/* 
    Employee Salary Calculation

    Given an array of employee objects, where each object contains the following properties: name, position, hoursWorked, and hourlyRate. 
    Write a function called calculateSalary that takes the array of employee objects as input and returns an array of employee objects with an additional property totalSalary, 
    which represents the total salary earned by each employee (hoursWorked * hourlyRate). 
*/

const employees = [
    { name: 'Alice', position: 'Manager', hoursWorked: 40, hourlyRate: 30 },
    { name: 'Bob', position: 'Developer', hoursWorked: 35, hourlyRate: 25 },
    { name: 'Charlie', position: 'Intern', hoursWorked: 20, hourlyRate: 15 }
];

function calculateSalary(employees) {
    /*
    Imperative Approach
    for (let x of employees) {
        x.totalSalary = x.hoursWorked * x.hourlyRate
    }
    return employees
    */

    //Declarative approach:
    return employees.map((x) => ({name: x.name,
                                position: x.position,
                                hoursWorked: x.hoursWorked,
                                hourlyRate: x.hourlyRate,
                                totalSalary: x.hoursWorked * x.hourlyRate
                                }))

}

/*
  Library Book Checkout System

  You are building a library book checkout system. You are given two arrays: books and borrowers. Each book is represented by an object with properties title and author. 
  Each borrower is represented by an object with properties name and books (an array of books borrowed by the borrower). Write a function called getAvailableBooks that takes the books and borrowers a
  arrays as input and returns an array of available books that are not borrowed by any borrower.

*/

  const books = [
    { title: 'Book 1', author: 'Author 1'},
    { title: 'Book 2', author: 'Author 2'},
    { title: 'Book 3', author: 'Author 3'},
    { title: 'Book 4', author: 'Author 4'},
    { title: 'Book 5', author: 'Author 4'}
  ];

  const borrowers = [
    { name: 'Borrower 1', books: ['Book 4'] },
    { name: 'Borrower 2', books: ['Book 5'] }
  ];

function getAvailableBooks(books, borrowers) {

    let borrowed = borrowers.map((x) => x.books).flat() 
    return books.filter((y) => !borrowed.includes(y.title));

    }

/* 
    Data Analysis - Sales Report

    You are given an array of sales objects, where each object contains the following properties: date, product, and quantity.
     Write a function called getTotalSalesByDate that takes the array of sales objects and a date as input and returns the total quantity of products sold on that date. 

*/

const sales = [
  { date: '2023-01-01', product: 'Product A', quantity: 10 },
  { date: '2023-01-01', product: 'Product B', quantity: 5 },
  { date: '2023-01-02', product: 'Product A', quantity: 8 },
  { date: '2023-01-02', product: 'Product C', quantity: 3 }
];

function getTotalSalesByDate(sales, date) {

    //Declarative approach: return sales.filter((x) => x.date == date).reduce((a,b) => a + b.quantity, 0)
    let result = 0

    for (let x of sales) {
        if (x.date === date) {
            result += x.quantity
        }
    }  

    return result

}

module.exports = {
    getTotalInventoryValue,
    isUserValid,
    calculateSalary,
    getAvailableBooks,
    getTotalSalesByDate
};